import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
// import { SearchGymPage } from '../pages/SearchGym/SearchGym';
import { HomePage } from '../home/home';
import { GSPage } from '../GS/GS';

@Component({
  selector: 'page-Gym',
  templateUrl: 'Gym.html'
})
export class GymPage {

  constructor(public navCtrl: NavController) {

  }


//   openSoccerSearch(){
// this.navCtrl.push(SearchGymPage);

//   }

openHome(){
  this.navCtrl.push(HomePage);
  }

openGS(){
    this.navCtrl.push(GSPage);
    }
  


}
