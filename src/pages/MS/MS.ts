import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MarriageHallsPage } from '../MarriageHalls/MarriageHalls';

@Component({
  selector: 'page-MS',
  templateUrl: 'MS.html'
})
export class MSPage {

  constructor(public navCtrl: NavController) {

  }

  images= [
    
    "SoccerGround.jpg",
     "Gym.png",
     "marraige.jpg"
           
    ];


    openMarriageHalls(){
      this.navCtrl.push(MarriageHallsPage);
      }


}
