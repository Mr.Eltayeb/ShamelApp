import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SearchSoccerPage } from '../SearchSoccer/SearchSoccer';
import { HomePage } from '../home/home';
import { SSPage } from '../SS/SS';


@Component({
  selector: 'page-SoccerFields',
  templateUrl: 'SoccerFields.html'
})
export class SoccerFieldsPage {

  username: string;
  phonenumber: string;

  constructor(public navCtrl: NavController) {

  }


  openSoccerSearch(){
this.navCtrl.push(SearchSoccerPage);

  }

openHome(){
  this.navCtrl.push(HomePage);
  }

  openSS(){
    this.navCtrl.push(SSPage);
    }
  

}
