import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SoccerFieldsPage } from '../SoccerFields/SoccerFields';
import L from "leaflet";
import { UserProvider } from '../../providers/user/user';

@Component({
  selector: 'page-SS',
  templateUrl: 'SS.html'
})
export class SSPage {
  map: L.Map;
  center: L.PointTuple;
 
  fromdate: string;
  todate: string;
  fromtime: string;
  totime: string;
  
  message:any;

  constructor(public navCtrl: NavController,     private userProv: UserProvider) {
  }

  NewBook(){
    this.userProv.BookProvider(this.fromdate,
       this.todate,
        this.fromtime,
        this.totime
        ).subscribe(data => {
          this.message = data;
        });

        if(this.message == ""){
console.log("");
        }

        else{
          console.log("");
        }
}


  // images= [
    
  //   "SoccerGround.jpg",
  //    "Gym.png",
  //    "marraige.jpg"
           
  //   ];
 
    ionViewDidLoad() {
      console.log('ionViewDidLoad MapPage');
      this.center = [28.644800, 77.216721];
      this.leafletMap();
    }
  
    leafletMap(){
      this.map = L.map('mapId', {
        center: this.center,
        zoom: 13
      });
  
      var position = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
        attribution: 'edupala.com © ionic LeafLet'
      }).addTo(this.map);
  
      var marker = new L.Marker(this.center);
      this.map.addLayer(marker);
  
      marker.bindPopup("<p>Tashi Delek.<p>Delhi</p>");
    } 


}
