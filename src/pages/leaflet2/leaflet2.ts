import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import L from "leaflet";

@Component({
  selector: 'page-leaflet2',
  templateUrl: 'leaflet2.html'
})
export class Leaflet2Page {
  map: L.Map;
  center: L.PointTuple;

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad MapPage');
    this.center = [28.644800, 77.216721];
    this.leafletMap();
   
  }

  leafletMap(){
    this.map = L.map('mapId2', {
      center: this.center,
      zoom: 13
    });

    var positron = L.tileLayer('http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {
      attribution: '© <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, © <a href="http://cartodb.com/attributions">CartoDB</a>'
    }).addTo(this.map);

    var marker = new L.Marker(this.center);
    this.map.addLayer(marker);

    marker.bindPopup("<p>Tashi Delek - Nmasta Bangalore.</p>");
   }
 }