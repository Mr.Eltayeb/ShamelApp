import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { LeafletPage } from '../leaflet/leaflet';
import { Leaflet2Page } from '../leaflet2/leaflet2';
import { AllMapPage } from '../allMap/allMap';

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = LeafletPage;
  tab2Root = Leaflet2Page;
  tab3Root = AllMapPage;

  constructor(public navCtrl: NavController) {

  }

}
