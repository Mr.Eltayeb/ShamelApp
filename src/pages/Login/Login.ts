import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';

@Component({
  selector: 'page-Login',
  templateUrl: 'Login.html'
})
export class LoginPage {

  phonenumber: string;
  password: string;

  message:any;


  constructor(public navCtrl: NavController,  private userProv: UserProvider) {

  }

  Login(){
    this.userProv.LoginProvider(
       this.phonenumber,
         this.password
        ).subscribe(data => {
          this.message = data;
        });

        if(this.message == ""){
console.log("");
        }

        else{
          console.log("");
        }
}



}
