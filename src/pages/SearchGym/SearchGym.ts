import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-SearchGym',
  templateUrl: 'SearchGym.html'
})
export class SearchGymPage {

  constructor(public navCtrl: NavController) {

  }

}
