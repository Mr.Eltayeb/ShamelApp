import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-Favorite',
  templateUrl: 'Favorite.html'
})
export class FavoritePage {

  constructor(public navCtrl: NavController) {

  }

}
