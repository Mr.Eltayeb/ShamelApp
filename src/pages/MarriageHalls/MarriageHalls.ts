import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
// import { SearchMarriageHallsPage } from '../pages/SearchMarriageHalls/SearchMarriageHalls';
import { HomePage } from '../home/home';
import { MSPage } from '../MS/MS';



@Component({
  selector: 'page-MarriageHalls',
  templateUrl: 'MarriageHalls.html'
})
export class MarriageHallsPage {

  constructor(public navCtrl: NavController) {

  }


//   openSoccerSearch(){
// this.navCtrl.push(SearchMarriageHallsPage);

//   }

openHome(){
this.navCtrl.push(HomePage);
}

openMS(){
  this.navCtrl.push(MSPage);
  }
  


}
