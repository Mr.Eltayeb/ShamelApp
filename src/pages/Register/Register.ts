import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';

@Component({
  selector: 'page-Register',
  templateUrl: 'Register.html'
})
export class RegisterPage {
 
  username: string;
  phonenumber: string;
  email: string;
  address: string;
  password: string;

  message:any;

  constructor(
    public navCtrl: NavController,
    private userProv: UserProvider
  ) {}

  NewRegister(){
      this.userProv.RegisterProvider(this.username,
         this.phonenumber,
          this.email,
          this.address,
           this.password
          ).subscribe(data => {
            this.message = data;
          });

          if(this.message == ""){
console.log("");
          }

          else{
            console.log("");
          }
  }


}
