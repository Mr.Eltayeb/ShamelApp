import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SoccerFieldsPage } from '../SoccerFields/SoccerFields';
import { MarriageHallsPage } from '../MarriageHalls/MarriageHalls';
import { GymPage } from '../Gym/Gym';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  openSoccerFields(){
    this.navCtrl.push(SoccerFieldsPage);

  }

  openGym(){
    this.navCtrl.push(GymPage);

  }
  
  openMarriageHalls(){
    this.navCtrl.push(MarriageHallsPage);

  }

}
