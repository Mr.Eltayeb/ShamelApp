import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { HttpClientModule } from  '@angular/common/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { SSPage } from '../pages/SS/SS';
// import { GSPage } from '../pages/GS/GS';
// import { MSPage } from '../pages/MS/MS';

import { FavoritePage } from '../pages/Favorite/Favorite';

// import { TabsPage } from '../pages/tabs/tabs';
// import { LeafletPage } from '../pages/leaflet/leaflet';
// import { Leaflet2Page } from '../pages/leaflet2/leaflet2';
// import { AllMapPage } from '../pages/allMap/allMap';

import { SearchSoccerPage } from '../pages/SearchSoccer/SearchSoccer';
// import { SearchMarriageHallsPage } from '../pages/SearchMarriageHalls/SearchMarriageHalls';
// import { SearchGymPage } from '../pages/SearchGym/SearchGym';

import { SoccerFieldsPage } from '../pages/SoccerFields/SoccerFields';
import { GymPage } from '../pages/Gym/Gym';
// import { MarriageHallsPage } from '../pages/MarriageHalls/MarriageHalls';

// import { LoginPage } from '../pages/Login/Login';
// import { RegisterPage } from '../pages/Register/Register';
 



import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { UserProvider } from '../providers/user/user';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    
    SoccerFieldsPage,
    GymPage,
    // MarriageHallsPage,
    
    SSPage,
    // GSPage,
    // MSPage,

    FavoritePage,
    
    SearchSoccerPage,
    // SearchMarriageHallsPage,
    // SearchGymPage
    
    // LoginPage,
    // RegisterPage,
    // TabsPage,
    // LeafletPage,
    // Leaflet2Page,
    // AllMapPage

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicImageViewerModule,
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,

    SoccerFieldsPage,
    GymPage,
    // MarriageHallsPage,

    SSPage,
    // GSPage,
    // MSPage,
 
    FavoritePage,

    SearchSoccerPage,
    // SearchMarriageHallsPage,
    // SearchGymPage
    
    // LoginPage,
    // RegisterPage,
    // TabsPage,
    // LeafletPage,
    // Leaflet2Page,
    // AllMapPage

  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UserProvider,

  ]
})
export class AppModule {}
