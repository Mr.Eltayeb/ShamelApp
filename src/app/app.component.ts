import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Icon } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { SSPage } from '../pages/SS/SS';
// import { GSPage } from '../pages/GS/GS';
// import { MSPage } from '../pages/MS/MS';

import { FavoritePage } from '../pages/Favorite/Favorite';

// import { TabsPage } from '../pages/tabs/tabs';
// import { LeafletPage } from '../pages/leaflet/leaflet';
// import { Leaflet2Page } from '../pages/leaflet2/leaflet2';
// import { AllMapPage } from '../pages/allMap/allMap';


import { SearchSoccerPage } from '../pages/SearchSoccer/SearchSoccer';
// import { SearchMarriageHallsPage } from '../pages/SearchMarriageHalls/SearchMarriageHalls';
// import { SearchGymPage } from '../pages/SearchGym/SearchGym';

import { SoccerFieldsPage } from '../pages/SoccerFields/SoccerFields';
import { GymPage } from '../pages/Gym/Gym';
// import { MarriageHallsPage } from '../pages/MarriageHalls/MarriageHalls';

// import { LoginPage } from '../pages/Login/Login';
// import { RegisterPage } from '../pages/Register/Register';



@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = SSPage;

  pages: Array<{title: string, component: any, icon: string}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'الرئيسية', component: HomePage, icon: "md-home" },
      { title: 'ملفي الشخصي', component: ListPage, icon: "md-person" },
      { title: 'المفضل', component: FavoritePage, icon: "md-heart" },
      { title: 'تسجيل خروج', component: ListPage, icon: "md-log-out" }
   
   
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
