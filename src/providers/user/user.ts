import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {
url="http://localhost:8080/api/auth/";
login= "login";
signup= "signup";
getUserById ="getUserById/"
getSoccerFieldById ="getSoccerFieldById/"
getGymById ="getGymById/"
getMarriageHallById ="getMarriageHallById/"
logout= "logout";

//full Path
loginUrl:string;
singupUrl:string;
getUserByidUrl:string;
getSoccerFieldByIdUrl:string;
getGymByIdUrl:string;
getMarriageHallByIdUrl:string;
logoutUrl:string;



  constructor(public http: HttpClient) {
this.loginUrl = this.url+this.login;
this.singupUrl= this.url+this.signup;
this.getUserByidUrl = this.url+this.getUserById;
this.getSoccerFieldByIdUrl = this.url+this.getSoccerFieldById;
this.getGymByIdUrl = this.url+this.getGymById;
this.getMarriageHallByIdUrl = this.url+this.getMarriageHallById;

this.logoutUrl = this.url+this.logout;
  
}


  RegisterProvider(username, phonenumber, email, address, password){
    let body = {
      "fullName" : username,
      "phone" : phonenumber,
      "email" : email,
      "address" : address,
      "password" : password
    }
  
      return this.http.post(this.singupUrl, body);
  }

  LoginProvider(phonenumber, password){
    let body = {
      "phone" : phonenumber,
      "password" : password
    }
  
      return this.http.post(this.loginUrl, body);
  }


  LogoutProvider(){

  }
 
  

  BookProvider(fromdate, todate, fromtime, totime){
    let body = {
      "fullName" : fromdate,
      "phone" : todate,
      "address" : fromtime,
      "password" : totime
    }
  
      return this.http.post(this.loginUrl, body);
  // }



  // GetUserByIdProvider(id){
  //   return this.http.get(this.getUserByidUrl+id)
  // }

  // GetSoccerFieldsByIdProvider(id){
  //   return this.http.get(this.getSoccerFieldByIdUrl+id)
  // }


  // GetGymByIdProvider(id){
  //   return this.http.get(this.getGymByIdUrl+id)
  // }

  // GetMarriageHallByIdProvider(id){
  //   return this.http.get(this.getMarriageHallByIdUrl+id)
  // }

}
}
